<?php
class MathGenerator
{
    protected $_difficulty;

    public function __construct($difficulty)
    {
        $this->_difficulty = $difficulty;
    }

    public function newProblem()
    {
        switch ($this->_difficulty) {
            case "1":
                return $this->additionProblem(20);
                break;

            case "2":
                return $this->additionProblem(999);
                break;

            case "3":
                return $this->multiplicationProblem(10);
                break;

            case "4":
                return $this->multiplicationProblem(100);
                break;

            case "5":
                return $this->base5MathProblem();
                break;

            default:
                throw new OutOfBoundsException("Invalid difficulty level");
                break;
        }
    }

    private function additionProblem($max)
    {
        $op1 = rand(0, $max);
        $op2 = rand(0, $max);

        return [
            'problem'  => "$op1 + $op2",
            'solution' => (string)($op1 + $op2)
        ];
    }

    private function multiplicationProblem($max)
    {
        $op1 = rand(0, $max);
        $op2 = rand(0, $max);

        return [
            'problem'  => "$op1 * $op2",
            'solution' => (string)($op1 * $op2)
        ];
    }

    private function base5MathProblem()
    {
        $op1 = rand(0, 125);
        $op2 = rand(0, 125);

        return [
            'problem'  => base_convert($op1, 10, 5) . " + " . base_convert($op2, 10, 5) . " in base 5",
            'solution' => (string)(base_convert($op1 + $op2, 10, 5))
        ];
    }
}