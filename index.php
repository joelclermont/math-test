<?php
require 'vendor/autoload.php';
require './config.php';

use Guzzle\Http\Client;

$client = new Client($config['base']);

// fetch items from the queue
$req = $client->get($config['queue']);
$rep = $req->send()->json();

$work_items = $rep['collection']['items'];

if (count($work_items) == 0) {
    die('No work to do. Take a break.');
}

// grab the first work item
$work_item = $work_items[0]['href'];
$req = $client->get($work_item);
$work_rep = $req->send()->json();

try {

    // signal work item start
    $req = $client->post($work_rep['start']);
    $rep = $req->send();

    // do the work
    $gen = new MathGenerator($work_rep['input']['difficulty']);
    $work_output = $gen->newProblem();

} catch (Exception $e) {
    
    // signal work item failure
    $req = $client->post($work_rep['fail']);
    $rep = $req->send();
    echo "We failed processing: $work_item. Don't worry though, we told the server. ";
    echo "Just between you and me, here's what happened: " . $e->getMessage();
    die();
    
}

// signal complete and send math problem
$req = $client->post($work_rep['complete'], [], $work_output);
$req->send();

echo 'Work item ' . $work_item . ' is complete. Output: ';
print_r($work_output);
echo 'Refresh to do more work!';